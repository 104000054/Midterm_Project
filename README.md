# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. [signUpWithEmailAndPassword]
    2. [createUserWithEmailAndPassword]
    3. [saveUserToDatabase]
    4. [signInWithEmailAndPassword]
* Other functions (add/delete)
    1. [snapshot]
    2. [initializeApp]
    3. [write]
    4. [limitToLast]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
這次的期中project做的是聊天室，主要是利用firebase的database功能，把資料存到firebase去，包含使用者資料以及聊天記錄等等，內含三個html檔以及各自的js檔、css檔。

index.html
這個是這次聊天室的主頁，這裡最主要提供使用者註冊以及登入的功能。我是參考上課的PPT做的，使用電子信箱註冊以及登入。原本還想要加上Google登入的功能，但一直沒辦法成功也de不出bug所以就放棄了。

主要的原理是先開兩個text的input吃帳號(email)跟密碼，之後再有兩個button讓使用者選擇要註冊或是登入。因為signUpWithEmailAndPassword跟signInWithEmailAndPassword的function已經寫好error了，所以其實不需要去做額外的處理，只要把input吃到的資料丟進去就好，這邊使用jquery的語法。之後用saveUserToDatabase的語法把新註冊的帳號密碼送到firebase建檔，那如果是要登入的話就會去firebase比對資料，之後就會跳到下一個網頁(lobby)。而登入失敗與註冊失敗的狀況跟原因其實error裡面都有，所以用alert來彈出一個視窗，告知錯誤訊息，如果正確的話也會跳出"Welcome!"跟"Sign Up!"的訊息提示。


lobby.html
這個是大廳，登入成功之後會到這邊來。這裡有所有成功註冊過的使用者，一樣是透過firebase製作的。這裡有一個input可以讓使用者輸入要跟誰聊天，一個開始聊天的button跟一個登出的button。如果是登出的話，會跳出提示訊息詢問是否要登出，是的話就會回到index，否的話則會留下。而按下開始聊天之後，會去比對input吃到的帳號是不是在列表中，如果沒有的話會跳出警告訊息，有的話則會進到下一個網頁(chatroom)。

chatroom
聊天室是這次的重點，也是我覺得很難做的部分。我是參考網路上的模板做的，它原本是一個單向控制電腦的聊天室，我把它改成雙向聊天的普通版本。比較重要的是要紀錄聊天內容，以達到"歷史紀錄"的功能，而每次聊天的內容都會被存到database去，所以每次開啟聊天室時都要先去database找資料。而目前登入的使用者(from)就是在index登入成功的email，聊天的對象(to)就是在lobby輸入的email，所以有了from跟to，就可以從database去過濾訊息，之後在js裡面做判斷，就可以把正確的歷史紀錄列出。之後使用append還有ScrollTop的語法，還有css的功能讓整個聊天室看起來更順暢。

其他的就是CSS的美化功能，包含背景，index的動畫，input跟button的設計，以及chatroom的背景等等。
## Security Report (Optional)
